package com.example.groceryexpirysaver;


public class Expiry {
    private int year, month, day;
    private String itemName;
    private int itemId;
    private int eId;

    /**
     * Default empty constructor to instantiate a new Expiry() object
     */
    public Expiry(){

    }

    /**
     * The constructory that instantiates and object of type Expiry with all the properties
     * passed in the parameters.
     * @param _year
     * @param _month
     * @param _day
     * @param _itemName
     * @param _eId
     * @param _itemId
     */
    public Expiry(int _year, int _month, int _day, String _itemName, int _eId, int _itemId){
        setYear(_year);
        setMonth(_month);
        setDay(_day);
        setItemName(_itemName);
        seteId(_eId);
        setItemId(_itemId);
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void seteId(int eId) {
        this.eId = eId;
    }

    public int geteId() {
        return eId;
    }

    public int getYear() {
        return year;
    }

    public int getMonth(){
        return month;
    }

    public int getDay() {
        return day;
    }

    public String getItemName() {
        return itemName;
    }
}
