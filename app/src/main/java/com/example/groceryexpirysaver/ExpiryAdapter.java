package com.example.groceryexpirysaver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class ExpiryAdapter extends ArrayAdapter<Expiry> {

    public ExpiryAdapter(Context c, ArrayList<Expiry> i){

        super(c, 0, i);
    }

    /**
     * This methods creates the textview properties for each element of type expiry
     * that the ExpiryAdapter is set to
     * @param pos
     * @param cv
     * @param p
     * @return
     */
    public View getView(int pos, View cv, ViewGroup p){
        Expiry i = getItem(pos);

        //Inflates/creates the expirylistview layout if it is null
        if (cv == null) {
            cv = LayoutInflater.from(getContext()).inflate(R.layout.expirylistview, p, false);
        }

        TextView name = cv.findViewById(R.id.name); //text view of product name in the current view (cv)
        TextView expiry = cv.findViewById(R.id.expiry); //text view of expiry date in the current view (cv)



        name.setText("Item: " + i.getItemName());
        expiry.setText("Expires: "+i.getDay()+"-"+i.getMonth()+"-"+i.getYear());

        return cv;
    }

}
