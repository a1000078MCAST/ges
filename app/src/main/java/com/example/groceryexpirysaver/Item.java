package com.example.groceryexpirysaver;

import java.util.Date;

public class Item {

    private int iId;
    private String sName;

    /**
     * Default empty constructor to instantiate a new Product() object
     */
    public Item(){

    }

    /**
     * The constructor that instantiates and object of type Item with all the properties
     * passed in the parameters.
     * @param _id
     * @param _name
     */
    public Item(int _id, String _name){
        setId(_id);
        setName(_name);
    }

    public void setId(int _id) {this.iId = _id;}
    public void setName(String _name) {this.sName = _name;}

    public int getiId() {return iId;}
    public String getsName() {return sName;}
}
