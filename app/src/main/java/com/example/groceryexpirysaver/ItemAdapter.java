package com.example.groceryexpirysaver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemAdapter extends ArrayAdapter<Item> {

    public ItemAdapter(Context c, ArrayList<Item> i){

        super(c, 0, i);
    }


    /**
     * This methods creates the textview properties for each element of type Item
     * that the ItemAdapter is set to
     * @param pos
     * @param cv
     * @param p
     * @return
     */
    public View getView(int pos, View cv, ViewGroup p){
        Item i = getItem(pos);


        //Inflates/creates the itemListView layout if it is null
        if (cv == null) {
            cv = LayoutInflater.from(getContext()).inflate(R.layout.itemlistview, p, false);
        }

        TextView iId = cv.findViewById(R.id.id); //text view of product id in the current view (cv)
        TextView sName = cv.findViewById(R.id.name); //text view of product name in the current view (cv)

        iId.setText("ID: " +(String.valueOf(i.getiId())));
        sName.setText("Item: "+(i.getsName()));

        return cv;
    }

}
