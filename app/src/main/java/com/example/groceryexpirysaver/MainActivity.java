package com.example.groceryexpirysaver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ItemAdapter itemAdapter;
    private ArrayList<Item> itemsArrayTest = new ArrayList<Item>();
    static boolean password = false;
    ArrayList<Item> itemsList = new ArrayList<Item>();
    TextView item;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;
    ListView lv;
    SharedPreferences sp;
    private static final String spName = "gesPreferences";
    private boolean activityCreated = false;


    /**
     * Overridden onPause method for MainActivity to save any
     * un submitted form values in the shared preferences whilst also
     * saving the date/time of when the activity has been paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(activityCreated){
            SimpleDateFormat sdf = new SimpleDateFormat("'Last Used on: 'yyyy-MM-dd 'at' HH:mm:ss");
            String currentTime = sdf.format(new Date());

            TextView inputItem = findViewById((R.id.itemName));
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putString("Item",  inputItem.getText().toString());
            spEditor.putString("LastUsed", currentTime);
            spEditor.commit();
        }

    }

    /**
     * Overridden onResume method for MainActivity to set any
     * previously saved un submitted form values in the shared preferences whilst also
     * showing the date/time of when the application has been last stopped/paused in a
     * snackbar
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(activityCreated){
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);

            if(sp.contains("Item")){
                item.setText(sp.getString("Item", ""));
            }

            if(sp.contains("LastUsed")){

                Snackbar.make(findViewById(R.id.nav_view), sp.getString("LastUsed", "N/A"), Snackbar.LENGTH_LONG).show();

            }
        }
    }

    /**
     * Overridden onStart method for MainActivity to set any
     * previously saved un submitted form values in the shared preferences whilst also
     * showing the date/time of when the application has been last stopped/paused in a
     * snackbar
     */
    @Override
    protected void onStart() {
        super.onStart();
        if(activityCreated){
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);

            if(sp.contains("Item")){
                item.setText(sp.getString("Item", ""));
            }

            if(sp.contains("LastUsed")){
                Snackbar.make(findViewById(R.id.nav_view), sp.getString("LastUsed", "N/A"), Snackbar.LENGTH_LONG).show();
            }
        }

    }


    /**
     * Overridden onStop method for MainActivity to save any
     * un submitted form values in the shared preferences whilst also
     * saving the date/time of when the activity has been stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (activityCreated) {
            SimpleDateFormat sdf = new SimpleDateFormat("'Last Used on: 'yyyy-MM-dd 'at' HH:mm:ss");
            String currentTime = sdf.format(new Date());

            TextView inputItem = findViewById((R.id.itemName));
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putString("Item", inputItem.getText().toString());
            spEditor.putString("LastUsed", currentTime);
            spEditor.commit();
        }
    }

    /**
     * The "Main" method that is called when the Activity has been loaded
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pinlockview);






        //checks if password is null (User not logged in through pinlock) and
        //if null promts the user with the pinlockscreen
        if (!password){
            PinLockView mPinLockView  = findViewById(R.id.pin_lock_view);
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mPinLockView.setPinLockListener(mPinLockListener);

        } else {
            //If the password is correct "1234" and the user has been logged in through the session
            //loads the activity_main layout

            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            setContentView(R.layout.activity_main);


            itemAdapter = new ItemAdapter( this, new ArrayList<Item>());
            lv = (ListView) findViewById(R.id.itemView);
            lv.setAdapter(itemAdapter);

            updateItemList();


            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView.setNavigationItemSelectedListener(this);
            item = findViewById((R.id.itemName));
            activityCreated = true;
        }
    }


    /**
     * Gets the items from the firebase realtime database located @ items/item
     * and calls the listener to query the database
     */
    private void updateItemList(){
        myRef = database.getReference("items/item");
        myRef.addListenerForSingleValueEvent(valueEventListener);
    }

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            itemAdapter.clear(); //removes all items present in the itemAdapter
            if (dataSnapshot.exists()){
                for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);
                    itemAdapter.add(item); //adds the new item to be displayed in the view
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.i("Cancelled","OK"); //no required. Logged only for testing
        }
    };

    /**
     * Deletes the item within the database using the ID
     * this can be retrieved by pressing on the ID within the view and an onclick listener
     * will set the text to the ID chosen
     * @param v
     */
    public void deleteItem(View v){
        String itemToDelete = item.getText().toString();
        if (itemToDelete.isEmpty()){
            Toast.makeText(getApplicationContext(), "No value has been entered. Please add an ID to delete", Toast.LENGTH_SHORT).show();
        } else{
            DatabaseReference itemdb = FirebaseDatabase.getInstance().getReference("items");
            itemdb.child("item").child(itemToDelete).removeValue();
            Toast.makeText(getApplicationContext(), "Item with ID: " + itemToDelete + " has been deleted", Toast.LENGTH_SHORT).show();
            updateItemList();
        }
        item.setText(""); //clears the item textview (Input box for new items/IDs to delete)
    }

    /**
     * Gets the ID of the item that has been clicked on (The actual ID must be clicked in this method)
     * I've used a different method in the second activity which uses the Adapter to get the position of the item
     * so no matter where the user clicks the ID is retrieved accordingly
     * @param v
     */
    public void onClickName(View v){
        TextView itemId = (TextView) v;
        int length = itemId.getText().toString().length();
        item.setText(itemId.getText().toString().substring(4,length));
    }

    /**
     * This method promts the user if the item textview is empty, otherwise this is stored in the database
     * as a new item with a randomly generated ID. This could have been done better by getting the last inserted ID and incrementing it
     * by 1 or 10 as to have a sequence of inputs but I've found some difficulties with the query.
     * @param v
     */
    public void addNewItem(View v){
        if (item.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "No value has been entered. Please add an item name!", Toast.LENGTH_SHORT).show();
        } else {
            Random rand = new Random();
            int newId = rand.nextInt(1000) * (rand.nextInt(20) + 11 - rand.nextInt(10));
            Toast.makeText(getApplicationContext(), "You have entered " + item.getText().toString() +  ". Genereated ID: " +newId, Toast.LENGTH_SHORT).show();

            Item newItem = new Item(newId, item.getText().toString());

            myRef.child(String.valueOf(newId)).setValue(newItem);
            item.setText("");
            updateItemList();
            /*
            itemsArrayTest.add(i);
            itemAdapter.clear();
            itemAdapter.addAll(itemsArrayTest);
            item.setText("");
            */
        }

    }

    /**
     * Used to restart the View of the MainActivity during the pinLock.
     */
    public void changeView(){
        Intent i = new Intent(this,MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
    }

    protected PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            if (pin.contains("1234")){ //If pin i 1234, set password = true and restart the Activity
                Toast.makeText(getApplicationContext(), "PIN Correct. Welcome!", Toast.LENGTH_LONG).show();
                Log.d("Pin","Valid Pin");
                password = true;
                changeView();
            } else { //if password is invalid, restart the activity (This will empty the inputted password)
                Toast.makeText(getApplicationContext(), pin +" is not Invalid. Please try again!", Toast.LENGTH_SHORT).show();
                changeView();
            }
        }

        @Override
        public void onEmpty() {
            Log.d("Pin", "Pin empty"); //Not Required but used for testing on input
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            //Not Required
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Creates the sideBar main
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This i the method called with the upper Menu dots are pressed.
     * Only exit can be used within this menu to terminate the application
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            onPause();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * This is the navigation method which is triggered whenever an item is pressed
     * The id of the presed item is checked and the relative activity is loaded
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.ItemDatabase) {
            Intent i = new Intent(this,MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
            // Handle the camera action
        } else if (id == R.id.ExpiryManagement) {
            Intent i = new Intent(this,SecondActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
        } else if (id == R.id.Recipes) {
            Intent i = new Intent(this,ThirdActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

