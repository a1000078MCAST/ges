package com.example.groceryexpirysaver;

public class Recipe {
    private String sImageURL;
    private String sName;
    private String sIngredients;

    /**
     * Default empty constructor to instantiate a new Recipe() object
     */
    public Recipe(){

    }

    /**
     * The constructor that instantiates and object of type Expiry with all the properties
     * passed in the parameters.
     * @param _ImageURL
     * @param _name
     * @param _Ingredients
     */
    public Recipe(String _ImageURL, String _name, String _Ingredients){
        setsImageURL(_ImageURL);
        setsName(_name);
        setsIngredients(_Ingredients);
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsImageURL() {
        return sImageURL;
    }

    public String getsIngredients() {
        return sIngredients;
    }


    public void setsImageURL(String sImageURL) {
        this.sImageURL = sImageURL;
    }

    public void setsIngredients(String sIngredients) {
        this.sIngredients = sIngredients;
    }

}
