package com.example.groceryexpirysaver;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    private Context recipeContext;
    private ArrayList<Recipe> recipeList;


    /**
     * Default contructor with paramteres to sset a new ArrayLit of Recipe objects and the context
     * @param _context
     * @param _recipeList
     */
    public RecipeAdapter(Context _context, ArrayList<Recipe> _recipeList){
        recipeContext = _context;
        recipeList = _recipeList;
    }


    /**
     * Creates a new RecyclerView with the layout of recipe_item
     * @param viewGroup
     * @param i
     * @return
     */
    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(recipeContext).inflate(R.layout.receipe_item, viewGroup, false);
        return new RecipeViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int i) {
        Recipe currentRecipe = recipeList.get(i);

        String recipeURL = currentRecipe.getsImageURL();
        String recipeName = currentRecipe.getsName();
        String recipeIngredients = currentRecipe.getsIngredients();

        recipeViewHolder.recipeName.setText(recipeName);
        recipeViewHolder.recipeIngredients.setText(recipeIngredients);


        Picasso.with(recipeContext).load(recipeURL).fit().centerInside().into(recipeViewHolder.recipeImage); //Loads an image from a URL and centres it within the CardView
    }

    /**
     * Returns the size of the items in the recipeList
     * @return
     */
    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder {
        public ImageView recipeImage;
        public TextView recipeName;
        public TextView recipeIngredients;

        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);
            recipeImage = itemView.findViewById(R.id.recipeImage);
            recipeName = itemView.findViewById(R.id.recipeName);
            recipeIngredients = itemView.findViewById(R.id.ingredients);
        }
    }
}
