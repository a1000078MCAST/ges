package com.example.groceryexpirysaver;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static android.app.PendingIntent.getActivity;

public class SecondActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef;
    private ItemAdapter itemAdapter;
    private Spinner ItemSpinner;
    private ArrayList<String> itemList = new ArrayList<>();
    static TextView dateView;
    private TextView selectedItem;
    private ExpiryAdapter expiryAdapter;
    static int selectedExpiryEID;
    ListView lv;
    SharedPreferences sp;
    private static final String spName = "gesPreferences";
    private boolean activityCreated = false;


    /**
     * Overridden onPause method for MainActivity to save any
     * un submitted form values in the shared preferences whilst also
     * saving the date/time of when the activity has been paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(activityCreated){
            SimpleDateFormat sdf = new SimpleDateFormat("'Last Used on: 'yyyy-MM-dd 'at' HH:mm:ss");
            String currentTime = sdf.format(new Date());

            TextView selectedItem = findViewById((R.id.selectedItem));
            TextView selectedDate = findViewById((R.id.dateSelection));
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putString("SelectedItem",  selectedItem.getText().toString());
            spEditor.putString("SelectedDate",  selectedDate.getText().toString());
            spEditor.putString("LastUsed", currentTime);
            spEditor.commit();
        }

    }

    /**
     * Overridden onResume method for MainActivity to set any
     * previously saved un submitted form values in the shared preferences whilst also
     * showing the date/time of when the application has been last stopped/paused in a
     * snackbar
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(activityCreated){
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            TextView selectedItem = findViewById((R.id.selectedItem));
            TextView selectedDate = findViewById((R.id.dateSelection));
            if(sp.contains("SelectedItem")){
                selectedItem.setText(sp.getString("SelectedItem", ""));
            }
            if(sp.contains("SelectedDate")){
                selectedDate.setText(sp.getString("SelectedDate", ""));
            }

            if(sp.contains("LastUsed")){
                Snackbar.make(findViewById(R.id.nav_view), sp.getString("LastUsed", "N/A"), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Overridden onStart method for MainActivity to set any
     * previously saved un submitted form values in the shared preferences whilst also
     * showing the date/time of when the application has been last stopped/paused in a
     * snackbar
     */
    @Override
    protected void onStart() {
        super.onStart();
        if(activityCreated){
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            TextView selectedItem = findViewById((R.id.selectedItem));
            TextView selectedDate = findViewById((R.id.dateSelection));
            if(sp.contains("SelectedItem")){
                selectedItem.setText(sp.getString("SelectedItem", ""));
            }
            if(sp.contains("SelectedDate")){
                selectedDate.setText(sp.getString("SelectedDate", ""));
            }

            if(sp.contains("LastUsed")){
                Snackbar.make(findViewById(R.id.nav_view), sp.getString("LastUsed", "N/A"), Snackbar.LENGTH_LONG).show();
            }
        }

    }

    /**
     * Overridden onStop method for MainActivity to save any
     * un submitted form values in the shared preferences whilst also
     * saving the date/time of when the activity has been stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
        if(activityCreated){
            SimpleDateFormat sdf = new SimpleDateFormat("'Last Used on: 'yyyy-MM-dd 'at' HH:mm:ss");
            String currentTime = sdf.format(new Date());

            TextView selectedItem = findViewById((R.id.selectedItem));
            TextView selectedDate = findViewById((R.id.dateSelection));
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putString("SelectedItem",  selectedItem.getText().toString());
            spEditor.putString("SelectedDate",  selectedDate.getText().toString());
            spEditor.putString("LastUsed", currentTime);
            spEditor.commit();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        expiryAdapter = new ExpiryAdapter( this, new ArrayList<Expiry>());
        lv = (ListView) findViewById(R.id.itemView2);
        lv.setAdapter(expiryAdapter);

        itemList.add("");
        updateItemList();

        ItemSpinner = findViewById(R.id.itemSpinner);
        ArrayAdapter<String> itemAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, itemList);
        ItemSpinner.setAdapter(itemAdapter);

        dateView = (TextView)findViewById(R.id.dateSelection);
        selectedItem = (TextView)findViewById((R.id.selectedItem));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Expiry e = expiryAdapter.getItem(position);
                selectedExpiryEID = e.geteId();
                showDeleteMenu(view);
            }
        });

        updateItemList2();


        ItemSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0){
                    selectedItem.setText(((TextView)view).getText().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //This will never be triggered
            }
        });


        activityCreated = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    public boolean showDeleteMenu(View view){
        PopupMenu popupMenu = new PopupMenu(getApplicationContext(), view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.confirmBTN){
                    deleteItem();
                    return true;
                }
                return false;
            }
        });


        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.deletemenu, popupMenu.getMenu());
        popupMenu.show();
        return false;
    }

    /**
     * Calls an Implicit intent to send an SMS through another application, while pasing a string
     * with all the item and their respecitve expiry date
     * @param v
     */
    public void sendSMS(View v)
    {
        Uri uri = Uri.parse("smsto:79979870");
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        String itemsInStock = "";

        for (int i = 0; i < expiryAdapter.getCount(); i++){
            Expiry e = expiryAdapter.getItem(i);
            itemsInStock += "\nItem: " + e.getItemName() + " - Expires: " +e.getDay() + "." + e.getMonth() + "." + e.getYear();
        }

        intent.putExtra("sms_body", itemsInStock);
        startActivity(intent);
    }


    /**
     * deletes and item from the firebase database and re updates the list of items visible within the
     * ListView
     */
    public void deleteItem(){
            DatabaseReference itemdb = FirebaseDatabase.getInstance().getReference("items");
            itemdb.child("expiry").child(""+selectedExpiryEID).removeValue();
            Toast.makeText(getApplicationContext(), "Item with ID: " + selectedExpiryEID + " has been deleted", Toast.LENGTH_SHORT).show();
            updateItemList2();
    }

    /**
     * Used to input a new expiry and an item into the firebase database
     * @param v
     */
    public void addNewItem(View v){
        if (selectedItem.getText().toString().isEmpty() || dateView.getText().toString().length() < 12){
            Toast.makeText(getApplicationContext(), "No Item Selected Or Date is Invalid", Toast.LENGTH_SHORT).show();
        } else {
            Random rand = new Random();
            int newId = rand.nextInt(100) * (rand.nextInt(20) + 7 - rand.nextInt(6));
            int year = Integer.parseInt(dateView.getText().toString().substring(6,10));
            int month = Integer.parseInt(dateView.getText().toString().substring(18,20).trim());
            int day = Integer.parseInt(dateView.getText().toString().substring(25, dateView.getText().toString().length()).trim());
            //Expiry(int _year, int _month, int _day, String _itemName, int _eId)
            String itemName = selectedItem.getText().toString();
            String[] split = itemName.split(" - ");
            itemName = split[0].substring(3, split[0].length()).trim();
            int itemId = Integer.parseInt(itemName);
            itemName = split[1].trim();

            Toast.makeText(this, "Added New Expiry "+day+"-"+month+"-"+year+" for " + itemName, Toast.LENGTH_LONG).show();
            Expiry newItemExpiry = new Expiry(year, month, day, itemName, newId, itemId);
            myRef = database.getReference("items/expiry");
            myRef.child(String.valueOf(newId)).setValue(newItemExpiry);

            updateItemList2();
        }

    }

    /**
     * Gets the expiry and items from the firebase realtime database located @ items/expiry
     * and calls the listener to query the database
     */
    private void updateItemList2(){
        myRef = database.getReference("items/expiry");
        myRef.addListenerForSingleValueEvent(valueEventListener2);
    }

    ValueEventListener valueEventListener2 = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            expiryAdapter.clear();
            if (dataSnapshot.exists()){
                for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Expiry expiry = snapshot.getValue(Expiry.class);
                    expiryAdapter.add(expiry);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.i("Cancelled","OK");
        }
    };

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateView.setText("Year: " + year+ " Month: " + (month+1) + " Day: " + day);
            Log.e("This Happened OK", "Set Text for date");
        }
    }






    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()){
                for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);
                    itemList.add("ID: "+ item.getiId()+ " - " + item.getsName().toString());
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.i("Cancelled","OK");
        }
    };

    /**
     * Gets the items from the firebase realtime database located @ items/item
     * and calls the listener to query the database
     */
    private void updateItemList(){
        myRef = database.getReference("items/item");
        myRef.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Creates the sideBar second
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    /**
     * This i the method called with the upper Menu dots are pressed.
     * Only exit can be used within this menu to terminate the application
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            onPause();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This is the navigation method which is triggered whenever an item is pressed
     * The id of the presed item is checked and the relative activity is loaded
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.ItemDatabase) {
            Intent i = new Intent(this,MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
            // Handle the camera action
        } else if (id == R.id.ExpiryManagement) {
            Intent i = new Intent(this,SecondActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
        } else if (id == R.id.Recipes) {
            Intent i = new Intent(this,ThirdActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
