package com.example.groceryexpirysaver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ThirdActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recipeRecView;
    private RecipeAdapter recipeAdapter;
    private ArrayList<Recipe> recipeList;
    private RequestQueue requestQueue;
    SharedPreferences sp;
    private static final String spName = "gesPreferences";
    private boolean activityCreated = false;


    /**
     * Overridden onPause method for MainActivity to save any
     * un submitted form values in the shared preferences whilst also
     * saving the date/time of when the activity has been paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(activityCreated){
            SimpleDateFormat sdf = new SimpleDateFormat("'Last Used on: 'yyyy-MM-dd 'at' HH:mm:ss");
            String currentTime = sdf.format(new Date());

            TextView ingredient = findViewById((R.id.ingredientText));
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putString("Ingredient",  ingredient.getText().toString());
            spEditor.putString("LastUsed", currentTime);
            spEditor.commit();
        }

    }

    /**
     * Overridden onResume method for MainActivity to set any
     * previously saved un submitted form values in the shared preferences whilst also
     * showing the date/time of when the application has been last stopped/paused in a
     * snackbar
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(activityCreated){
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            TextView ingredient = findViewById((R.id.ingredientText));
            if(sp.contains("Ingredient")){
                ingredient.setText(sp.getString("Ingredient", ""));
            }

            if(sp.contains("LastUsed")){
                Snackbar.make(findViewById(R.id.nav_view), sp.getString("LastUsed", "N/A"), Snackbar.LENGTH_LONG).show();
            }


        }
    }

    /**
     * Overridden onStart method for MainActivity to set any
     * previously saved un submitted form values in the shared preferences whilst also
     * showing the date/time of when the application has been last stopped/paused in a
     * snackbar
     */
    @Override
    protected void onStart() {
        super.onStart();
        if(activityCreated){
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            TextView ingredient = findViewById((R.id.ingredientText));
            if(sp.contains("Ingredient")){
                ingredient.setText(sp.getString("Ingredient", ""));
            }

            if(sp.contains("LastUsed")){
                Snackbar.make(findViewById(R.id.nav_view), sp.getString("LastUsed", "N/A"), Snackbar.LENGTH_LONG).show();
            }
        }

    }

    /**
     * Overridden onStop method for MainActivity to save any
     * un submitted form values in the shared preferences whilst also
     * saving the date/time of when the activity has been stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
        if(activityCreated){
            SimpleDateFormat sdf = new SimpleDateFormat("'Last Used on: 'yyyy-MM-dd 'at' HH:mm:ss");
            String currentTime = sdf.format(new Date());

            TextView ingredient = findViewById((R.id.ingredientText));
            sp = getSharedPreferences(spName, Context.MODE_PRIVATE);
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putString("Ingredient",  ingredient.getText().toString());
            spEditor.putString("LastUsed", currentTime);
            spEditor.commit();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        recipeRecView = findViewById(R.id.recycler_view);
        recipeRecView.setHasFixedSize(true);
        recipeRecView.setLayoutManager(new LinearLayoutManager(this));

        recipeList = new ArrayList<Recipe>();

        requestQueue = Volley.newRequestQueue(this);
        parseJSON();


        activityCreated = true;
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * The below i used to attach the method to a button and calling parseJSON() within it
     * @param v
     */
    public void searchRecipe(View v){
        parseJSON();
    }


    /**
     * The below method calls a json request to the Recipe API with the value of the item passed by the user
     * This is then paresed into a JSONObject and within it a JSONArray with key "hits" is retrieved. This is then further split and manipulated to get
     * the required values. Since there were multiple nested JSONObjects and Arrays 2 for loops had to be used. The retrieved data is stored into appropriate variables
     * and then appended to the recipeList which is binded to the recipeAdapter
     */
    private void parseJSON(){
        TextView query = (TextView) findViewById(R.id.ingredientText);

        String jsonURL = "https://api.edamam.com/search?q="+query.getText().toString()+"&app_id=414458eb&app_key=b21173ee3d12c25e77f95dc3da70e4d2";

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, jsonURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray hits = response.getJSONArray("hits");
                            recipeList.clear();
                            for (int i = 0; i < hits.length(); i++){
                                JSONObject objectHit = hits.getJSONObject(i);

                                JSONObject recipe = objectHit.getJSONObject("recipe");

                                String recipeName = recipe.getString("label");

                                String recipeImage = recipe.getString("image");

                                String recipeIngredients = "\nIngredients:";

                                JSONArray recipeArray = recipe.getJSONArray("ingredientLines");
                                for (int y = 0; y < recipeArray.length(); y++){
                                    recipeIngredients += "\n";
                                    recipeIngredients += recipeArray.getString(y);

                                }

                                recipeList.add(new Recipe(recipeImage,recipeName,recipeIngredients));
                            }

                            recipeAdapter = new RecipeAdapter(ThirdActivity.this, recipeList);
                            recipeRecView.setAdapter(recipeAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonRequest);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Creates the sideBar second
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    /**
     * This i the method called with the upper Menu dots are pressed.
     * Only exit can be used within this menu to terminate the application
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            onPause();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This is the navigation method which is triggered whenever an item is pressed
     * The id of the presed item is checked and the relative activity is loaded
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.ItemDatabase) {
            Intent i = new Intent(this,MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
            // Handle the camera action
        } else if (id == R.id.ExpiryManagement) {
            Intent i = new Intent(this,SecondActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
        } else if (id == R.id.Recipes) {
            Intent i = new Intent(this,ThirdActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
